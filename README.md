# T-Mobile Home Internet Tools
![](icon.png)

Set of programs to make managing T-Mobile 5G Wifi Gateways easier

Notice: This has only been tested and known to work with the Nokia "trashcan" Gateway!

## Here is the Nokia "Trashcan" Gateway:
![](trashcan.png)

## The programs:

`tmocli`: Command line interface for working with the gateway

![](screenshots/tmocli.png)

`tmotop`: Text interface for working with the gateway

![](screenshots/tmotop.png)

`tmo-webui` Mobile friendly web interface for working with the gateway

![](screenshots/webui.png)
![](screenshots/webui-mobile.png)

`gatewaymon`: Background process that monitors the gateway for network issues (reboots the gateway if needed) and monitors the gateway's cell signal

`tmo-hosts`: (Work in progress) Background process that makes other computers on the network acessible by their hostnames

## Installation
### Arch Linux and others based on it
Use your AUR helper to install `tmobile-internet-tools`

## Troubleshooting
"Error: Not authorized" errors coming from nmcli in gatewaymon: Make sure your user is in the 'network' group and use `visudo` to add the following to `/etc/sudoers`: `%network ALL=(ALL) NOPASSWD: /usr/bin/nmcli`

## Disclaimer: I am in no way affiliated with T-Mobile. This program is my own work.

## Planned Features:
- [ ] Adjust gateway's network settings from tmotop and the web UI
- [ ] Get Arcadyan gateway and port the programs to its API
- [ ] Allow web interface to auto-refresh gateway info
- [ ] Reverse proxy to native gateway web interface
- [ ] Background process that tracks the IPs of other hosts (These gateways are DHCP only)
- [x] Text UI to show metrics and settings
- [x] Background process that reboots the gateway when needed
- [x] Command line interface to work with the gateway
- [x] Web interface that shows metrics and settings
