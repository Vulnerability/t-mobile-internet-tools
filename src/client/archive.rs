use super::tracker::SignalTracker;
use crate::LOG_DIR;
use std::{
    env,
    fs::{self, OpenOptions},
    collections::HashSet,
    io::Write
};
use rand::{thread_rng, Rng};
use zip::{
    ZipArchive,
    write::{FileOptions, ZipWriter}
};
use chrono::{Datelike, Timelike, Local};
use lazy_static::lazy_static;

lazy_static! {
    // place to extract existing data archives
    static ref CACHE_DIR: String = format!("/tmp/tmo-arccache-{}", env::var("USER").expect("What is your name again?"));

    // archive directory
    static ref ARC_DIR: String = format!("{}/archive", *LOG_DIR);
}

// clear all logs
pub fn clear_all_logs() {
    for file in fs::read_dir(&*LOG_DIR).unwrap().flatten() {
        let name_t = file.file_name();
        let name = name_t.to_str().unwrap();
        let path = file.path();
        let l = name.len();
        if l < 4 { continue; }
        if &name[l - 4..] == ".log" {
            fs::remove_file(path).unwrap_or(());
        }
    }
}

// archive gateway monitor logs
pub fn archive_gwmonlogs() {
    let mut archive = DataArchive::load("log.zip");

    // copy existing gateway monitor logs into cache
    for file in fs::read_dir(&*LOG_DIR).expect("Unable to read log directory!").flatten() {
        let name_t = file.file_name();
        let name = name_t.to_str().unwrap();
        let path = format!("{}/{name}", *LOG_DIR);
        if name.contains("gatewaymon") {
            archive.move_log_file(&path, name);
        }
    }
}

// archive signal tracker data
pub fn archive_sig_tracker() {
    let mut archive = DataArchive::load("signal_tracker.zip");
    archive.move_sig_tracker();
}

// extract and return the raw signal tracker files
pub fn get_archived_signal_trackers() -> Vec<Vec<u8>> {
    let archive = DataArchive::load("signal_tracker.zip");
    let mut sig_trackers: Vec<Vec<u8>> = Vec::new();
    for file in fs::read_dir(&archive.extr_dir).unwrap().flatten() {
        let name_t = file.file_name();
        let name = name_t.to_str().unwrap();
        let path = format!("{}/{name}", archive.extr_dir);
        sig_trackers.push(fs::read(path).unwrap());
    }

    sig_trackers
}

// get date listing of archived logs
pub fn get_log_dates() -> Option<Vec<(u32, u32, u32)>> {
    let archive = DataArchive::load("log.zip");
    let mut dates = Vec::new();
    for file in archive.list_files().iter().rev() {
        let mut split1 = file.split('.');
        let mut split2 = split1.next().unwrap().split('-');
        split2.next();
        dates.push((
            split2.next().unwrap().parse().unwrap(),
            split2.next().unwrap().parse().unwrap(),
            split2.next().unwrap().parse().unwrap()
        ));
    }
    let mut uniques = HashSet::new();
    dates.retain(|d| uniques.insert(*d));
    dates.sort_unstable();
    dates.reverse();
    drop(archive);
    if dates.is_empty() { None }
    else { Some(dates) }
}

// get archived log by date
pub fn get_log(date: (u32, u32, u32)) -> Option<String> {
    let archive = DataArchive::load("log.zip");
    fs::read_to_string(format!("{}/gatewaymon-{}-{}-{}.log", archive.extr_dir, date.0, date.1, date.2)).ok()
}

struct DataArchive {
    extr_dir: String,
    zip_name: String,
    modified: bool
}

impl DataArchive {
    // load a data archive
    fn load(zip_name: &str) -> DataArchive {
        let mut rand = thread_rng();
        let extr_dir = format!("{}-{}", *CACHE_DIR, rand.gen::<u128>());
        fs::create_dir_all(&extr_dir).unwrap();
        env::set_current_dir(&extr_dir).unwrap();
        if env::set_current_dir(&*ARC_DIR).is_ok() {
            if let Ok(file) = fs::File::open(zip_name) {
                let mut zip = ZipArchive::new(file).unwrap();
                zip.extract(&extr_dir).unwrap();
            }
        }

        DataArchive {
            extr_dir,
            zip_name: zip_name.to_owned(),
            modified: false
        }
    }

    // add existing signal tracker file into cache
    fn move_sig_tracker(&mut self) {
        env::set_current_dir(&*LOG_DIR).unwrap();
        let date = Local::now().date();
        let time = Local::now().time();
        let file_name = format!("signal_tracker-{:04}-{:02}-{:02}-{:02}{:02}{:02}", date.year(), date.month(), date.day(), time.hour(), time.minute(), time.second());
        fs::copy("signal_tracker", format!("{}/{file_name}", self.extr_dir)).unwrap();
        let mut tracker = SignalTracker::get(false);
        tracker.clear();
        self.modified = true;
    }

    // add log file to archive
    fn move_log_file(&mut self, in_path: &str, name: &str) {
        let data = fs::read(in_path).unwrap();
        let mut new_file = OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(format!("{}/{name}", self.extr_dir))
            .unwrap();

        new_file.write_all(&data).unwrap();
        new_file.flush().unwrap();
        fs::remove_file(in_path).unwrap_or(());
        self.modified = true;
    }

    fn list_files(&self) -> Vec<String> {
        fs::read_dir(&self.extr_dir).unwrap().map(|file| {
            let name_t = file.unwrap().file_name();
            name_t.to_str().unwrap().to_string()
        }).collect::<Vec<String>>()
    }
}

impl Drop for DataArchive {
    fn drop(&mut self) {
        if self.modified {
            fs::create_dir_all(&*ARC_DIR).unwrap();
            env::set_current_dir(&*ARC_DIR).unwrap();
            let mut zipfile = ZipWriter::new(fs::File::create(&self.zip_name).unwrap());
            let options = FileOptions::default().compression_method(zip::CompressionMethod::Deflated).unix_permissions(0o755);

            for file in fs::read_dir(&self.extr_dir).unwrap().flatten() {
                let name_t = file.file_name();
                let name = name_t.to_str().unwrap();
                let path = format!("{}/{name}", self.extr_dir);
                let file_buf = fs::read(path).unwrap();
                zipfile.start_file(name, options).unwrap();
                zipfile.write_all(&file_buf).unwrap();
            }
        }
        fs::remove_dir_all(&self.extr_dir).unwrap();
    }
}