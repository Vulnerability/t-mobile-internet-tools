use rust_utils::logging::LogLevel;
use sha2::{Sha256, Digest};
use crate::{LOG, client::ClientError};

mod nokia;
pub use nokia::NokiaToken;

fn sha_256_hash(val1: &str, val2: &str) -> String {
    let mut hasher = Sha256::new();
    let string_to_hash = format!("{val1}:{val2}");
    hasher.update(&string_to_hash);
    let output = hasher.finalize();
    base64::encode(&output[..])
}

fn encode_as_url(b64: String) -> String {
    let mut out = String::new();
    for c in b64.chars() {
        match c {
            '+' => out.push('-'),
            '/' => out.push('_'),
            '=' => out.push('.'),
            _ => out.push(c),
        }
    }
    out
}

fn parse_json_res(source: &str) -> Result<json::JsonValue, ClientError> {
    match json::parse(source) {
        Ok(j) => Ok(j),
        Err(why) => {
            LOG.line(LogLevel::Error, format!("JSON Error: {why}"), false);
            LOG.line(LogLevel::Error, "Received input:", false);
            for line in source.lines() {
                LOG.line(LogLevel::Error, line, false);
            }
            Err(ClientError::LoginFailed)
        }
    }
}