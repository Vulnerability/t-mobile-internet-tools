use super::{
    token::NokiaToken,
    AuthMode,
    ClientError,
    ClientResult,
    GatewayClient,
    parse_json
};
use std::{
    sync::Arc,
    time::Duration
};
use reqwest::{
    blocking::Client,
    cookie::Jar
};
use crate::{
    devices::DeviceList,
    config::{DeviceConfig, GlobalConfig, Config},
    stats::{AdvancedGatewayStats, CellInfo, GatewayInfo, NetworkInfo}
};

pub struct TrashcanClient {
    client: Client,
    jar: Arc<Jar>,
}

#[allow(clippy::new_without_default)]
impl TrashcanClient {
    // create a new client
    pub fn new() -> TrashcanClient {
        let jar = Arc::new(Jar::default());
        let client = Client::builder().connect_timeout(Duration::from_secs(10)).timeout(Duration::from_secs(10)).cookie_store(true).cookie_provider(jar.clone()).build().unwrap();
        TrashcanClient {
            client,
            jar
        }
    }

    // POST request to the gateway
    fn post(&self, res: &str, mode: AuthMode) -> ClientResult<()> {
        let config = GlobalConfig::load();
        let addr = format!("http://{}/{res}", config.gateway_ip);
        let req = self.client.post(addr);
        let req_r = match mode {
            AuthMode::Web => {
                let token = self.gen_web_token()?;
                req.form(&token.get_data()).send()
            },
            AuthMode::None => req.send()
        };

        match req_r {
            Ok(_) => Ok(()),
            Err(why) => Err(ClientError::GatewayUnreachable(why.to_string()))
        }
    }

    // GET request to the gateway
    fn get(&self, res: &str, mode: AuthMode) -> ClientResult<String> {
        let config = GlobalConfig::load();
        let addr = format!("http://{}/{res}", config.gateway_ip);
        let req = self.client.get(addr);
        let req_r = match mode {
            AuthMode::Web => {
                let _token = self.gen_web_token()?;
                req.send()
            },

            AuthMode::None => req.send()
        };

        match req_r {
            Ok(response) => Ok(response.text().unwrap()),
            Err(why) => Err(ClientError::GatewayUnreachable(why.to_string()))
        }
    }

    fn gen_web_token(&self) -> ClientResult<NokiaToken> {
        let config = GlobalConfig::load();
        let token_r = NokiaToken::get_web(&self.client, &config.username, &config.password);
        if let Ok(token) = token_r {
            token.put_cookies_in_jar(&self.jar);
            Ok(token)
        }
        else {
            token_r
        }
    }
}

impl GatewayClient for TrashcanClient {
    // request the gateway to reboot (no log output)
    fn reboot_gateway_no_log(&mut self) -> Result<(), ClientError> {
        self.post("reboot_web_app.cgi", AuthMode::Web)
    }

    // get a list of devices and their ipv4 addresses connected to the gateway
    fn get_devices(&self) -> ClientResult<DeviceList> {
        let dev_config = DeviceConfig::load();
        let response = self.get("dashboard_device_info_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response);
        DeviceList::get(json?, &dev_config)
    }

    // get the 5G metrics
    fn get_5g_stats(&self) -> ClientResult<CellInfo> {
        let response = self.get("fastmile_radio_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;
        let bars = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPStrengthIndexCurrent"].as_u8().expect("Value not found!");
        let strength = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().expect("Value not found!");
        let band = json["cell_5G_stats_cfg"][0]["stat"]["Band"].as_str().expect("Value not found!").to_string();

        Ok(CellInfo {
            band,
            bars,
            strength
        })
    }

    // get the LTE metrics
    fn get_4g_stats(&self) -> ClientResult<CellInfo> {
        let response = self.get("fastmile_radio_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;
        let bars = json["cell_5G_stats_cfg"][0]["stat"]["RSRPStrengthIndexCurrent"].as_u8().expect("Value not found!");
        let strength = json["cell_5G_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().expect("Value not found!");
        let band = json["cell_LTE_stats_cfg"][0]["stat"]["Band"].as_str().expect("Value not found!").to_string();

        Ok(CellInfo {
            band,
            bars,
            strength
        })
    }

    // get the connection status as a boolean
    fn get_conn_status(&self) -> bool {
        let response = self.get("fastmile_radio_status_web_app.cgi",  AuthMode::None);
        if response.is_err() {
            return false;
        }
        let json_res = parse_json(&response.unwrap());

        if let Ok(json) = json_res {
            let val = json["connection_status"][0]["ConnectionStatus"].as_usize();
            if val.is_none() {
                return false;
            }
            return val.unwrap() == 1
        }
        false
    }

    // get info about the gateway
    fn get_info(&self) -> ClientResult<GatewayInfo> {
        let response = self.get("device_status_web_app.cgi", AuthMode::Web)?;
        let json = parse_json(&response)?;
        let response = self.get("fastmile_statistics_status_web_app.cgi", AuthMode::Web)?;
        let sim_json = parse_json(&response)?;
        let vendor = json["Vendor"].as_str().unwrap().to_string();
        let ser_num = json["SerialNumber"].as_str().unwrap().to_string();
        let hw_ver = json["HardwareVersion"].as_str().unwrap().to_string();
        let sw_ver = json["SoftwareVersion"].as_str().unwrap().to_string();
        let uptime = json["UpTime"].as_usize().unwrap();
        let imei = sim_json["network_cfg"][0]["IMEI"].as_str().unwrap().parse().unwrap();
        Ok(GatewayInfo {
            vendor,
            ser_num,
            hw_ver,
            sw_ver,
            uptime,
            imei
        })
    }

    // get advanced gateway stats
    fn get_adv_stats(&self) -> ClientResult<AdvancedGatewayStats> {
        let response = self.get("fastmile_radio_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;

        // APN info
        let apn_name = json["apn_cfg"][0]["APN"].as_str().unwrap().to_string();
        let apn_ip4 = json["apn_cfg"][0]["X_ALU_COM_IPAddressV4"].as_str().unwrap().to_string();
        let apn_ip6 = json["apn_cfg"][0]["X_ALU_COM_IPAddressV6"].as_str().unwrap().to_string();

        // 5G cell info
        let band_5g = json["cell_5G_stats_cfg"][0]["stat"]["Band"].as_str().unwrap().to_string();
        let rsrp_5g = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap();
        let snr_5g = json["cell_5G_stats_cfg"][0]["stat"]["SNRCurrent"].as_i32().unwrap();
        let rsrq_5g = json["cell_LTE_stats_cfg"][0]["stat"]["RSRQCurrent"].as_i32().unwrap();
        let rssi = json["cell_LTE_stats_cfg"][0]["stat"]["RSSICurrent"].as_i32().unwrap();

        // 4G cell info
        let band_4g = json["cell_LTE_stats_cfg"][0]["stat"]["Band"].as_str().unwrap().to_string();
        let rsrp_4g = json["cell_5G_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap();
        let snr_4g = json["cell_LTE_stats_cfg"][0]["stat"]["SNRCurrent"].as_i32().unwrap();
        let rsrq_4g = json["cell_5G_stats_cfg"][0]["stat"]["RSRQCurrent"].as_i32().unwrap();

        // SIM info
        let response = self.get("fastmile_statistics_status_web_app.cgi", AuthMode::Web)?;
        let sim_json = parse_json(&response)?;
        let imsi = sim_json["sim_cfg"][0]["IMSI"].as_str().unwrap().parse().unwrap();
        let iccid = sim_json["sim_cfg"][0]["ICCID"].as_str().unwrap().parse().unwrap();
        let imei = sim_json["network_cfg"][0]["IMEI"].as_str().unwrap().parse().unwrap();

        Ok(AdvancedGatewayStats {
            apn_name,
            apn_ip4,
            apn_ip6,
            band_5g,
            rsrp_5g,
            snr_5g,
            rsrq_5g,
            rssi,
            band_4g,
            rsrp_4g,
            snr_4g,
            rsrq_4g,
            imsi,
            iccid,
            imei
        })
    }

    // get download data usage
    fn get_download(&self) -> ClientResult<u128> {
        let response = self.get("fastmile_radio_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;
        Ok(json["cellular_stats"][0]["BytesReceived"].as_u64().expect("Value not found!") as u128)
    }

    // get upload data usage
    fn get_upload(&self) -> ClientResult<u128> {
        let response = self.get("fastmile_radio_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;
        Ok(json["cellular_stats"][0]["BytesSent"].as_u64().expect("Value not found!") as u128)
    }

    // get list of active networks
    fn get_networks(&self) -> ClientResult<Vec<NetworkInfo>> {
        let response = self.get("statistics_status_web_app.cgi", AuthMode::None)?;
        let json = parse_json(&response)?;
        let eth_networks = json["LAN"].members();
        let mut networks: Vec<NetworkInfo> = vec![];
        for (i, network_raw) in eth_networks.enumerate() {
            let id = network_raw["Name"].as_str().expect("Value not found!").to_string();
            let name = format!("Ethernet {}", i + 1);
            let upload = network_raw["BytesSent"].as_u64().expect("Value not found!") as u128;
            let download = network_raw["BytesReceived"].as_u64().expect("Value not found!") as u128;
            let enable_str = &network_raw["Enable"].as_u32().expect("Value not found!");

            networks.push(NetworkInfo {
                id,
                name,
                upload,
                download,
                enabled: (*enable_str == 1)
            })
        }

        let wifi_networks = json["WLAN"].members();

        for network_raw in wifi_networks {
            let id = network_raw["Name"].as_str().expect("Value not found!").to_string();
            let name = network_raw["SSID"].as_str().expect("Value not found!").to_string();
            let upload = network_raw["BytesSent"].as_u64().expect("Value not found!") as u128;
            let download = network_raw["BytesReceived"].as_u64().expect("Value not found!") as u128;
            let enable = &network_raw["Enable"].as_u32().expect("Value not found!");

            networks.push(NetworkInfo {
                id,
                name,
                upload,
                download,
                enabled: (*enable == 1)
            })
        }

        Ok(networks)
    }
}