use std::{
    process, thread, env,
    time::{Instant, Duration},
};
use chrono::{Timelike, Local};
use rust_utils::{
    utils,
    logging::LogLevel
};
use crate::{
    LOG, DEBUG, CLIENT,
    config::{GatewayConfig, Config},
    client::{self, tracker::*}
};

// gateway monitoring daemon (background process)
// this will check the network environment once every minute and will tell the gateway to reboot if there are any problems
pub fn init() {
    LOG.line_basic("Starting T-Mobile Gateway Monitor...", true);
    ctrlc::set_handler(|| {
        LOG.line_basic("Shutting down...", true);
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    // check if the user is in the group 'network'
    let getent = utils::run_command("getent", false, ["group", "network"]);
    let username = env::var("USER").expect("What is your name again?");
    if !getent.output.contains(&username) {
        LOG.line(LogLevel::Error, "Your user is not in the network group!", true);
        LOG.line(LogLevel::Error, format!("To fix, run the following command as root: usermod -a -G network {username}"), true);
        return;
    }

    // is the notify-send command available?
    let can_notify = {
        let cmd = utils::run_command("notify-send", false, ["-v"]);
        cmd.success
    };

    let mut client = CLIENT.write().unwrap();
    let mut startup = true;
    let mut reboot_time = Instant::now();
    let mut collect = true;
    LOG.line_basic("Startup complete.", true);
    LOG.line(*DEBUG, "Debug logging enabled", true);

    let gw_config = GatewayConfig::load();
    loop {
        let now = Local::now();

        // if network issues are encountered, request the gateway to restart
        // if not configured to reboot the gateway, just reset the wifi connection
        if client::is_valid_net() {
            if client::check_net_env(now.second() == 0) != 0 {
                let mut tracker = SignalTracker::get(false);
                LOG.line(LogLevel::Info, "Why the hell is the gateway causing issues again?!", true);
                let cur_name = client::nmcli(["-t", "-f", "NAME", "con", "show", "--active"], true);
                if !startup && reboot_time.elapsed().as_secs() <= 300 {
                    power_off_notify(can_notify);
                }
                
                match client.get_tracker_entry(now, true) {
                    Ok(entry) => tracker.add_entry(entry),
                    Err(error) => LOG.line(LogLevel::Error, format!("Error getting signal stats: {error}"), true)
                };

                if gw_config.reboot_on_err {
                    match client.reboot_gateway(true) {
                        Ok(()) => {
                            reboot_time = Instant::now();
                            startup = false;
                            wait_for_net(&cur_name);
                            LOG.line_basic("Gateway has been rebooted", true);
                        },

                        Err(error) => {
                            LOG.line(LogLevel::Error, format!("Error rebooting gateway: {error}"), true);
                            power_off_notify(can_notify);
                            wait_for_net(&cur_name);
                        }
                    };
                }
                else {
                    wait_for_net(&cur_name);
                    thread::sleep(Duration::from_secs(15));
                }
            }

            // collect the signal stats of the gateway every 15 minutes
            if now.minute() % 15 == 0 && collect {
                let mut tracker = SignalTracker::get(false);
                match client.get_tracker_entry(now, false) {
                    Ok(entry) => tracker.add_entry(entry),
                    Err(error) => LOG.line(LogLevel::Error, format!("Error getting signal stats: {error}"), true)
                };
                collect = false
            }
            else if now.minute() % 15 != 0 {
                collect = true;
            }

            // request the gateway to reboot at a specified time
            if now.hour() == gw_config.reboot_hr && now.minute() == gw_config.reboot_min && gw_config.auto_reboot {
                let cur_name = client::nmcli(["-t", "-f", "NAME", "con", "show", "--active"], true);
                match client.reboot_gateway(true) {
                    Ok(()) => {
                        wait_for_net(&cur_name);
                        LOG.line(LogLevel::Info, "Gateway has been rebooted", true);
                        thread::sleep(Duration::from_secs(30));
                    },

                    Err(error) => {
                        LOG.line(LogLevel::Error, format!("Error rebooting gateway: {error}"), true);
                        power_off_notify(can_notify);
                    }
                };
            }
        }
        else {
            LOG.line(LogLevel::Warn, "This is not a T-Mobile Home Internet network or connection has been lost", true);
            while !client::is_valid_net() { }
        }
    }
}

fn power_off_notify(can_notify: bool) {
    LOG.line(LogLevel::Warn, "You may want to power off your gateway", true);
    if can_notify {
        utils::run_command("notify-send", false,
            [
                "-a",
                "T-Mobile Gateway Monitor",
                "-i",
                "/usr/share/tmobile-internet-tools/webui/favicon.png",
                "You may want to power off your gateway"
            ]
        );
    }
}

// turn off wifi and reenable again and wait for the network to come back online
fn wait_for_net(cur_name: &str) {
    client::nmcli(["radio", "wifi", "off"], true);
    thread::sleep(Duration::from_secs(1));
    client::nmcli(["radio", "wifi", "on"], true);
    LOG.line_basic("Waiting for network to come online again...", true);
    let conn_attempt = Instant::now();
    thread::sleep(Duration::from_secs(5));
    while client::check_net_env(true) != 0 {
        if conn_attempt.elapsed().as_secs() % 60 == 0 {
            client::nmcli(["con", "up", cur_name], false);
        }
    }
    thread::sleep(Duration::from_secs(30));
}