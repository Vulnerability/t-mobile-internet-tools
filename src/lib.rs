pub mod config;
pub mod devices;
pub mod stats;
pub mod client;

use lazy_static::lazy_static;
use rust_utils::{utils, logging::{Log, LogLevel}};
use std::{env, sync::RwLock};
use client::BoxedClient;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");

lazy_static! {
    pub static ref LOG: Log = Log::get(&utils::get_execname(), "tmo-tools");
    pub static ref DEBUG: LogLevel = should_debug();
    pub static ref PRINT_STDOUT: bool = utils::get_execname().as_str() != "tmotop";
    pub static ref CLIENT: RwLock<BoxedClient> = RwLock::new(BoxedClient::new());
    pub static ref LOG_DIR: String = format!("{}/.local/share/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!"));
}

// are we in debug mode?
fn should_debug() -> LogLevel {
    // get the runtime arguments
    let args: Vec<String> = env::args().collect();

    // should debug messages be shown?
    if args.is_empty() { LogLevel::Debug(false) }
    else if let Some(string) = args.get(1) { LogLevel::Debug(string == "-d" && utils::get_execname().as_str() != "tmocli") }
    else { LogLevel::Debug(false) }
}