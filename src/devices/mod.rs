use std::fmt;
use crate::{
    client,
    config::DeviceConfig
};
use serde_derive::{Serialize, Deserialize};

#[derive(Clone, Eq, PartialEq, Deserialize, Serialize)]
pub struct DeviceList {
    pub devices: Vec<Device>
}

impl DeviceList {
    // generate a device list from json response from the gateway
    pub fn get(json: json::JsonValue, hst_config: &DeviceConfig) -> Result<DeviceList, client::ClientError> {
        let empty = "".to_owned();
        let device_array = json["device_cfg"].members();
        let mut devices: Vec<Device> = vec![
            Device {
                name: "tmo-gateway".to_string(),
                alias: "".to_string(),
                ip: [192, 168, 12, 1],
                mac_addr: [0, 0, 0, 0, 0, 0],
                active: true
            }
        ];
        let aliases = &hst_config.aliases;
        for device_raw in device_array {
            let name = device_raw["HostName"].as_str().unwrap().to_string();
            let mut ip_raw = device_raw["IPAddress"].as_str().unwrap().split('.');
            let mut mac_raw = device_raw["MACAddress"].as_str().unwrap().split(':');
            let alias = aliases.get(&format!("{name}{}", hst_config.tld)).unwrap_or(&empty);
            let a: u8 = match ip_raw.next().unwrap().parse() {
                Ok(val) => val,
                Err(_) => continue
            };
            let b: u8 = ip_raw.next().unwrap().parse().unwrap();
            let c: u8 = ip_raw.next().unwrap().parse().unwrap();
            let d: u8 = ip_raw.next().unwrap().parse().unwrap();

            let e: u8 = match hex::decode(mac_raw.next().unwrap()) {
                Ok(hex) => hex[0],
                Err(_) => continue
            };

            let f: u8 = hex::decode(mac_raw.next().unwrap()).unwrap()[0];
            let g: u8 = hex::decode(mac_raw.next().unwrap()).unwrap()[0];
            let h: u8 = hex::decode(mac_raw.next().unwrap()).unwrap()[0];
            let i: u8 = hex::decode(mac_raw.next().unwrap()).unwrap()[0];
            let j: u8 = hex::decode(mac_raw.next().unwrap()).unwrap()[0];

            let active = device_raw["Active"].as_u8().unwrap() != 0;
            devices.push(
                Device {
                    name: format!("{name}{}", hst_config.tld),
                    alias: alias.to_string(),
                    ip: [a, b, c, d],
                    mac_addr: [e, f, g, h, i, j],
                    active
                }
            )
        }
        
        Ok(DeviceList { devices })
    }
}

impl fmt::Display for DeviceList {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for device in &self.devices {
            let mut name = &device.name;
            if !device.alias.is_empty() {
                name = &device.alias;
            }
            let formatted_ip = format!("{}.{}.{}.{}", device.ip[0], device.ip[1], device.ip[2], device.ip[3]);
            writeln!(f, "{} {}", formatted_ip, name)?;
        }
        Ok(())
    }
}

// represents a device connected to the gateway
#[derive(Clone, Eq, PartialEq, Deserialize, Serialize)]
pub struct Device {
    name: String,
    alias: String,
    ip: [u8; 4],
    mac_addr: [u8; 6],
    pub active: bool
}

impl Device {
    pub fn get_ip(&self) -> String {
        format!("{}.{}.{}.{}", self.ip[0], self.ip[1], self.ip[2], self.ip[3])
    }

    pub fn get_name(&self) -> String {
        if self.alias.is_empty() {
            self.get_real_name()
        }
        else {
            self.alias.clone()
        }
    }

    pub fn get_real_name(&self) -> String {
        self.name.clone()
    }

    pub fn get_mac_addr(&self) -> String {
        format!(
            "{:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}",
            self.mac_addr[0],
            self.mac_addr[1],
            self.mac_addr[2],
            self.mac_addr[3],
            self.mac_addr[4],
            self.mac_addr[5]
        )
    }

    pub fn is_gateway(&self) -> bool {
        self.mac_addr == [0, 0, 0, 0, 0, 0]
    }
}