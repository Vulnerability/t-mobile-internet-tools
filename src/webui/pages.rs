use super::*;
use actix_web::{HttpResponse, Result};
use std::{
    fmt::{Display, Write},
    collections::HashSet
};
use tmo_tools::{
    VERSION,
    stats::format_bytes,
    client::{self, tracker::SignalTracker}
};

#[derive(Deserialize)]
pub struct DiagOptions {
    log_date: Option<String>,
    stats_date: Option<String>,
    show_arc_logs: Option<String>,
    show_arc_stats: Option<String>
}

#[derive(Deserialize)]
pub struct LogDate {
    month: u32,
    day: u32,
    year: u32,
    #[serde(default)]
    archive: bool
}

// each of these functions returns a web page (including iframe pages)

// main page
pub async fn index() -> Result<HttpResponse> {
    LOG.line_basic("Loading index page...", true);
    let stats_t = STATS.read().ok_or_exit();
    let stats = stats_t.as_ref().ok_or(ValueError)?;
    let info_t = INFO.read().ok_or_exit();
    let info = info_t.as_ref().ok_or(ValueError)?;

    // get gateway info
    LOG.line_basic("Getting gateway info...", true);
    let disp_str = info.to_string();
    let mut info_html = String::new();
    for line in disp_str.lines() {
        write!(info_html, "<h3>{line}</h3>").unwrap_or(());
    }

    // get networks list
    LOG.line_basic("Getting active networks...", true);
    let mut net_html = String::new();
    for network in &stats.networks {
        if network.enabled {
            let total_bytes = network.upload + network.download;
            let download_per = if total_bytes == 0 { 0.0 } else { (network.download as f64 / total_bytes as f64) * 100f64 };

            write!(net_html,
                "\n<p>\n\
                    <b>{}</b><br>\n\
                    <span style=\"color: magenta;\">Down: {}</span> <span style=\"color: cyan;\">Up: {}</span>\n\
                </p>\n\
                {}",
                network.name,
                format_bytes(network.download),
                format_bytes(network.upload),
                data_bar(download_per.ceil() as u32, 200, 25, false)
            ).unwrap_or(());
        }
    }

    // get connected device list
    LOG.line_basic("Getting connected devices...", true);
    let mut dev_html = String::new();
    
    for device in &stats.devices.devices {
        let mut mac_addr = String::new();
        if !device.is_gateway() {
            writeln!(mac_addr, "MAC Address: {}", device.get_mac_addr()).unwrap_or(());
        }

        write!(dev_html,
            "<p>\n\
                <b style=\"color: magenta;\">{}</b><br>\n\
                IP Address: {}<br>\n\
                {mac_addr}\
            </p>",
            device.get_name(),
            device.get_ip()
        ).unwrap_or(());
    }

    // get data usage stats
    LOG.line_basic("Getting data usage stats...", true);
    let total_bytes = stats.upload + stats.download;
    let download_per = (stats.download as f64 / total_bytes as f64) * 100f64;
    let data_usage = format!(
        "<p>\n\
            Total: {}<br>\n\
            <span style=\"color: magenta;\">Download: {}</span><br>\n\
            <span style=\"color: cyan;\">Upload: {}</span>\n\
        </p>\n\
        {}\
        <p>Disclaimer: This is the data usage since the last restart</p>",
        format_bytes(total_bytes),
        format_bytes(stats.download),
        format_bytes(stats.upload),
        data_bar(download_per.ceil() as u32, 500, 50, true)
    );

    Ok(html_response(
        format!(
            "<h1 style=\"text-align: center;\">Gateway Status</h1>\n\
            <div id=\"trashcan_info\">\n\
                <h2 style=\"text-align: center;\">Gateway Info</h2>\n\
                <img src=\"trashcan.png\" id=\"trashcan\" class=\"center\">\n\
                {info_html}\n\
            </div>\n\
            <div id=\"stats\">\n\
                <h2>Gateway Stats</h2>\n\
                <button class=\"hide_button\">Connection</button>\n\
                <div class=\"hideable\" id=\"conn_stats_frame\">\n\
                    <iframe src=\"/conn_stats\" frameBorder=\"0\"></iframe>\n\
                </div><br>\n\
                <button class=\"hide_button\">Data Usage</button>\n\
                <div class=\"hideable\">\n\
                    <br>{data_usage}\n\
                </div><br>\n\
                <button class=\"hide_button\">Active Networks</button>\n\
                <div class=\"hideable\">\n\
                    <br>{net_html}\n\
                </div><br>\n\
                <button class=\"hide_button\">Connected Devices</button>\n\
                <div class=\"hideable\">\n\
                    <br>{dev_html}\n\
                </div>\n\
            </div>"
        ),
        false,
        true
    ))
}

// diagnostics page 
pub async fn diag(web::Query(ops): web::Query<DiagOptions>) -> Result<HttpResponse> {
    LOG.line_basic("Loading diagnostics page...", true);
    let show_arc_logs = if let Some(ref string) = ops.show_arc_logs {
        string == "on"
    }
    else {
        false
    };
    let show_arc_stats = if let Some(ref s) = ops.show_arc_stats {
        s == "on"
    }
    else {
        false
    };
    let arc_stats_cb = if show_arc_stats { " checked" } else { "" };
    let arc_logs_cb = if show_arc_logs { " checked" } else { "" };
    let arc_stats_hidden = if show_arc_stats { "on" } else { "" };
    let arc_logs_hidden = if show_arc_logs { "on" } else { "" };
    let sig_tracker = SignalTracker::get(show_arc_stats);
    let gws_dates = sig_tracker.get_dates();
    let gws_date = {
        let mut date_t = gws_dates[0];
        if let Some(stats_date) = ops.stats_date {
            if !stats_date.is_empty() {
                let mut split = stats_date.split(' ');
                if let (Ok(year), Ok(month), Ok(day)) = (split.next().unwrap().parse::<u32>(), split.next().unwrap().parse::<u32>(), split.next().unwrap().parse::<u32>()) {
                    date_t = (year, month, day)
                }
            }
        }
        date_t
    };

    let dates = client::get_log_dates(show_arc_logs);
    let date = {
        let mut date_t = dates[0];
        if let Some(log_date) = ops.log_date {
            if !log_date.is_empty() {
                let mut split = log_date.split(' ');
                if let (Ok(year), Ok(month), Ok(day)) = (split.next().unwrap().parse::<u32>(), split.next().unwrap().parse::<u32>(), split.next().unwrap().parse::<u32>()) {
                    date_t = (year, month, day)
                }
            }
        }
        date_t
    };

    let mut log_date_selector = String::new();
    for log_date in dates {
        writeln!(log_date_selector,
            "<option value=\"{} {} {}\"{}>{}/{}/{}</option>",
            log_date.0,
            log_date.1,
            log_date.2,
            if log_date == date { " selected=\"selected\"" } else { "" },
            log_date.1,
            log_date.2,
            log_date.0
        ).unwrap_or(());
    }

    let mut gws_date_selector = String::new();
    for gws_date_t in gws_dates {
        writeln!(gws_date_selector,
            "<option value=\"{} {} {}\"{}>{}/{}/{}</option>",
            gws_date_t.0,
            gws_date_t.1,
            gws_date_t.2,
            if gws_date_t == gws_date { " selected=\"selected\"" } else { "" },
            gws_date_t.1,
            gws_date_t.2,
            gws_date_t.0
        ).unwrap_or(());
    }

    Ok(html_response(
        format!(
            "<h1 style=\"text-align: center;\">Gateway Diagnostics</h1>\n\
            <h3>Network Status: <span style=\"{}</span></h3>\n\
            <button class=\"hide_button\">Gateway Monitor Log</button>\n\
            <div class=\"hideable\">\n\
                <iframe style=\"height: 500px; background-color: black;\" src=\"/diag/gatewaymon_log?year={log_year}&month={log_month}&day={log_day}&archive={show_arc_logs}\" frameBorder=\"0\" style=\"background-color: black;\"></iframe>\n\
                <form action=\"/diag\">\n\
                    Select Date: \n\
                    <select name=\"log_date\">\n\
                        {log_date_selector}\
                    </select>\n\
                    <input type=\"hidden\" name=\"stats_date\" value=\"{gws_year} {gws_month} {gws_day}\">\n\
                    <br>\n\
                    <input type=\"checkbox\" name=\"show_arc_logs\" {arc_logs_cb}>Show Archived Logs\n\
                    <input type=\"hidden\" name=\"show_arc_stats\" value=\"{arc_stats_hidden}\">\n\
                    <br>\n\
                    <input type=\"submit\" value=\"Submit\">\n\
                </form>\n\
            </div>\n\
            <button class=\"hide_button\">Connection Stats Log</button>\n\
            <div class=\"hideable\">\n\
                <iframe style=\"height: 500px;\" src=\"/diag/gateway_stats_log?year={gws_year}&month={gws_month}&day={gws_day}&archive={show_arc_stats}\" frameBorder=\"0\"></iframe>\n\
            </div>\n\
            <button class=\"hide_button\">Connection Stats Visual</button>\n\
            <div class=\"hideable\" style=\"width: 100%; overflow: auto;\">\n\
                {conn_stat_vis}\n\
            </div>\n\
            <form action=\"/diag\">\n\
                Select Date: \n\
                <select name=\"stats_date\">\n\
                    {gws_date_selector}\
                </select>\n\
                <input type=\"hidden\" name=\"log_date\" value=\"{log_year} {log_month} {log_day}\">\n\
                <br>\n\
                <input type=\"checkbox\" name=\"show_arc_stats\"{arc_stats_cb}>Show Archived Stats\n\
                <input type=\"hidden\" name=\"show_arc_logs\" value=\"{arc_logs_hidden}\">\n\
                <br>\n\
                <input type=\"submit\" value=\"Submit\">\n\
            </form>",
            if client::check_net_env(true) == 0 { "color: green;\">Ok" } else { "color: red;\">Offline" },
            gws_year = gws_date.0,
            gws_month = gws_date.1,
            gws_day = gws_date.2,
            log_year = date.0,
            log_month = date.1,
            log_day = date.2,
            conn_stat_vis = conn_stat_vis(&sig_tracker, gws_date)
        ),
        false,
        false
    ))
}

// connection stat visual
fn conn_stat_vis(tracker: &SignalTracker, date: (u32, u32, u32)) -> String {
    let mut shapes = String::new();
    let mut times = HashSet::new();
    for (drawn_rects, entry) in tracker.iter_entries_by_date(date).enumerate() {
        // if the strength is some ungodly small or large number, skip this entry
        if entry.info_5g.strength < -130 || entry.info_5g.strength > -70 || entry.info_4g.strength < -130 || entry.info_4g.strength > -70 || entry.date != date {
            continue;
        }

        let height_5g = ((((-entry.info_5g.strength) as f32 - 70.) / 60.) * 200.).ceil() as u32;
        let height_4g = ((((-entry.info_4g.strength) as f32 - 70.) / 60.) * 200.).ceil() as u32;
        let locx = drawn_rects * 10 + 140;

        let rect_5g = format!(
            "<rect width=\"10\" height=\"{}\" x=\"{locx}\" y=\"{height_5g}\" style=\"fill: {};\" />\n",
            200 - height_5g,
            if entry.reboot { "red" } else { "cyan" },
        );

        let rect_4g = format!(
            "<rect width=\"10\" height=\"{}\" x=\"{locx}\" y=\"{height_4g}\" style=\"fill: {};\" />\n",
            200 - height_4g,
            if entry.reboot { "rgb(170,44,17)" } else { "rgb(0,153,204)" }
        );

        if height_5g < height_4g {
            shapes.push_str(&rect_5g);
            shapes.push_str(&rect_4g);
        }
        else {
            shapes.push_str(&rect_4g);
            shapes.push_str(&rect_5g);
        }

        if times.insert(entry.time.0) {
            writeln!(shapes, "<text x=\"{locx}\" y=\"230\" fill=\"white\">{}</text>", entry.time.0).unwrap_or(());
        }
    }
    format!(
        "<svg height=\"310px\" width=\"{width}px\">\n\
            {shapes}\
            <line x1=\"140\" y1=\"0\" x2=\"140\" y2=\"200\" style=\"stroke: white; stroke-width:1\" />\n\
            <line x1=\"140\" y1=\"200\" x2=\"{width}\" y2=\"200\" style=\"stroke: white; stroke-width:1\" />\n\
            <text x=\"5\" y=\"220\" fill=\"white\">Time:</text>\n\
            <text x=\"5\" y=\"120\" fill=\"white\">Strength</text>\n\
            <text x=\"20\" y=\"137\" fill=\"white\">(dBm)</text>\n\
            <text x=\"100\" y=\"20\" fill=\"white\">-70</text>\n\
            <text x=\"100\" y=\"65\" fill=\"white\">-85</text>\n\
            <text x=\"90\" y=\"110\" fill=\"white\">-100</text>\n\
            <text x=\"90\" y=\"155\" fill=\"white\">-115</text>\n\
            <text x=\"90\" y=\"200\" fill=\"white\">-130</text>\n\
            <text x=\"5\" y=\"240\" fill=\"white\">Legend:</text>\n\
            <rect x=\"5\" y=\"245\" width=\"20\" height=\"20\" style=\"fill: cyan;\" />\n\
            <rect x=\"25\" y=\"245\" width=\"20\" height=\"20\" style=\"fill: red;\" />\n\
            <rect x=\"5\" y=\"265\" width=\"20\" height=\"20\" style=\"fill: rgb(0,153,204);\" />\n\
            <rect x=\"25\" y=\"265\" width=\"20\" height=\"20\" style=\"fill: rgb(170,44,17)\" />\n\
            <text x=\"50\" y=\"260\" fill=\"white\">5G Signal</text>\n\
            <text x=\"50\" y=\"280\" fill=\"white\">4G (LTE) Signal</text>\n\
            <text x=\"50\" y=\"300\" fill=\"white\">Red indicates the gateway was rebooted</text>\n\
        </svg>",
        width = tracker.num_entries(date) * 10 + 140
    )
}

// gateway monitor log viewer
pub async fn gatewaymon_log(web::Query(LogDate { month, day, year, archive }): web::Query<LogDate>) -> HttpResponse {
    LOG.line_basic("Generating gateway monitor log...", true);
    let log = client::get_log_plain((year, month, day), archive);
    let mut log_html = String::new();
    for line in log.lines() {
        writeln!(log_html,
            "<span style=\"color: {};\">{line}</span><br>",
            if line.contains("DEBUG]") { "cyan" }
            else if line.contains("WARN]") { "yellow" }
            else if line.contains("ERROR]") || line.contains("FATAL]") { "red" }
            else { "green" }
        ).unwrap_or(());
    }
    
    html_response(
        format!(
            "<div id=\"gwmonlog\" style=\"background-color: black;\">\n\
                {log_html}\n\
            </div>"
        ),
        true,
        false
    )
}

// gateway stats log viewer
pub async fn gateway_stats_log(web::Query(LogDate { month, day, year, archive }): web::Query<LogDate>) -> Result<HttpResponse> {
    LOG.line_basic("Generating gateway stats log...", true);
    let sig_tracker = SignalTracker::get(archive);
    let mut table_fmt = String::new();
    for entry in sig_tracker.iter_entries_by_date((year, month, day)) {
        writeln!(table_fmt,
            "<tr>\n\
                <td>{:02}:{:02}</td>\n\
                <td>{}</td>\n\
                <td>Band: {} Strength: {} dBm <span style=\"color: magenta;\">{}</span></td>\n\
                <td>Band: {} Strength: {} dBm <span style=\"color: magenta;\">{}</span></td>\n\
                <td>{}</td>\n\
            </tr>",
            entry.time.0,
            entry.time.1,
            if entry.conn_stat { "Yes" } else { "No" },
            entry.info_4g.band,
            entry.info_4g.strength,
            entry.info_4g.get_bars_vis(),
            entry.info_5g.band,
            entry.info_5g.strength,
            entry.info_5g.get_bars_vis(),
            if entry.reboot { "Yes" } else { "No" }
        ).unwrap_or(());
    }
    
    Ok(html_response(
        format!(
            "<table>\n\
                <tr>\n\
                    <th>Time</th>\n\
                    <th>Connected?</th>\n\
                    <th>LTE Signal</th>\n\
                    <th>5G Signal</th>\n\
                    <th>Rebooted?</th>\n\
                </tr>\n\
                {table_fmt}\
            </table>"
        ),
        true,
        false
    ))
}

// help page
pub async fn help() -> HttpResponse {
    html_response(
        format!(
            "<h1 style=\"text-align: center;\">T-Mobile Home Internet Tools</h1>\n\
            <p>\n\
                Version {VERSION}<br>\n\
                <a href=\"http://192.168.12.1\">Gateway web interface</a> (This will not work when accessing the this interface remotely!)<br>\n\
                Log file directory: {home}/.local/share/tmo-tools<br>\n\
                Settings directory: {home}/.config/tmo-tools<br>\n\
                <a href=\"https://gitlab.com/NoahJelen/tmobile-internet-tools\">Source Code</a><br>\n\
                <a href=\"https://gitlab.com/NoahJelen/tmobile-internet-tools/-/issues\">Report a bug</a><br>\n\
            </p>",
            home = env::var("HOME").expect("Where the hell is your home folder?!")
        ),
        false,
        false
    )
}

// advanced gateway stats
pub async fn adv_stats() -> Result<HttpResponse> {
    LOG.line_basic("Getting gateway stats...", true);
    let stats = client_req(|| CLIENT.read().ok_or_exit().get_adv_stats())?;
    Ok(html_response(
        format!(
            "<h1 style=\"text-align: center;\">Advanced Stats</h1>\n\
            <h2>APN Info</h2>\n\
            <p>\n\
                APN Name: {}<br>\n\
                IPv4 Address: {}<br>\n\
                IPv6 Address: {}<br>\n\
            </p>\n\
            <h2>5G Cell Info</h2>\n\
            <p>\n\
                Band: {}<br>\n\
                RSRP: {} dBm<br>\n\
                SNR: {} dB<br>\n\
                RSRQ: {} dB<br>\n\
                RSSI: {} dBm<br>\n\
            </p>\n\
            <h2>4G Cell Info</h2>\n\
            <p>\n\
                Band: {}<br>\n\
                RSRP: {} dBm<br>\n\
                SNR: {} dB<br>\n\
                RSRQ: {} dB<br>\n\
            </p>\n\
            <h2>SIM Info</h2>\n\
            <p>\n\
                IMSI: {}<br>\n\
                ICCID: {}<br>\n\
                Gateway IMEI: {}\n\
            </p>",
            stats.apn_name,
            stats.apn_ip4,
            stats.apn_ip6,
            stats.band_5g,
            stats.rsrp_5g,
            stats.snr_5g,
            stats.rsrq_5g,
            stats.rssi,
            stats.band_4g,
            stats.rsrp_4g,
            stats.snr_4g,
            stats.rsrq_4g,
            stats.imsi,
            stats.iccid,
            stats.imei
        ),
        false,
        false
    ))
}

// settings page
pub async fn config() -> Result<HttpResponse> {
    LOG.line_basic("Loading settings page...", true);
    let config = GlobalConfig::load();
    let gw_config = GatewayConfig::load();
    let dev_config = DeviceConfig::load();
    let valid_networks = &config.valid_networks;
    let mut net_html = String::new();
    for network in valid_networks {
        if !net_html.is_empty() {
            net_html.push_str("<br>\n");
        }
        net_html.push_str(network);
    }

    Ok(html_response(
        format!(
            "<h1 style=\"text-align: center;\" >Settings</h1>\n\
            <form id=\"settings\" action=\"/config/save_cfg\" method=\"post\">\n\
                Gateway IP Address:<br>\n\
                <input name=\"gw_ip\" type=\"text\" value=\"{}\"><br>\n\
                Password:<br>\n\
                <input id=\"pass\" name=\"password\" type=\"password\" value=\"{}\"><br>\n\
                <input type=\"checkbox\" onclick=\"toggle_password()\"> Show Password<br>\n\
                Add Network Name:<br>\n\
                <input name=\"new_net\" type=\"text\"><br>\n\
                <b>Known Networks:</b><br>\n\
                {net_html}\n\
                <h2>Gateway Settings</h2>\n\
                <input type=\"checkbox\" name=\"reboot_on_err\"{}> Reboot on network issues<br>\n\
                <input type=\"checkbox\" name=\"auto_reboot\"{}><label> Auto-Reboot</label><br>\n\
                Auto-Reboot Time (24 Hour Time):<br><input type=\"number\" name=\"reboot_hr\" min=\"0\" max=\"23\" value=\"{}\" style=\"width: 40px;\">:<input type=\"number\" name=\"reboot_min\" min=\"0\" max=\"59\" style=\"width: 40px;\" value=\"{:02}\"><br>\n\
                <h2>Connected Device Settings</h2>\n\
                Top level domain:<br>\n\
                <input name=\"tld\" type=\"text\" value=\"{}\"><br>\n\
                Gateway Name:<br>\n\
                <input name=\"gw_name\" type=\"text\" value=\"{}\"><br>\n\
                <input type=\"submit\" value=\"Save\">\n\
            </form>\n\
            <button onclick=\"conf_reboot()\">Reboot Gateway</button>\n\
            <script>\
                function toggle_password(){{\
                    var pass_entry=document.getElementById(\"pass\");\
                    if(pass_entry.type==\"password\"){{\
                        pass_entry.type=\"text\";\
                    }}\
                    else{{\
                        pass_entry.type=\"password\";\
                    }}\
                }}\
                async function conf_reboot(){{\
                    if(confirm(\"Are you sure? Your network will be down for about 5 minutes.\")){{\
                        await fetch(\"/config/reboot_gateway\");\
                    }}\
                }}\
            </script>",
            config.gateway_ip,
            config.password,
            if gw_config.reboot_on_err { " checked" } else { "" },
            if gw_config.auto_reboot { " checked" } else { "" },
            gw_config.reboot_hr,
            gw_config.reboot_min,
            dev_config.tld,
            dev_config.gateway_name
        ),
        false,
        false
    ))
}

// connection stats widget
pub async fn conn_stats() -> Result<HttpResponse> {
    LOG.line_basic("Getting connection status...", true);
    let stats_t = STATS.read().ok_or_exit();
    let stats = stats_t.as_ref().ok_or(ValueError)?;
    Ok(html_response(
        format!(
            "<div class=\"clearfix\">\n\
                <div id=\"stats_5g\">\n\
                    5G:<br>\n\
                    Band: {}<br>\n\
                    Strength: {} dBm<br>\n\
                    <img src=\"bars/{}.png\"><br>\n\
                    <p class=\"center\">5G</p>\n\
                </div>\n\
                <div id=\"stats_4g\">\n\
                    4G (LTE):<br>\n\
                    Band: {}<br>\n\
                    Strength: {} dBm<br>\n\
                    <img src=\"bars/{}.png\"><br>\n\
                    <p class=\"center\">LTE</p>\n\
                </div>\n\
            </div>",
            stats.info_5g.band,
            stats.info_5g.strength,
            stats.info_5g.bars,
            stats.info_4g.band,
            stats.info_4g.strength,
            stats.info_4g.bars,
        ),
        true,
        false
    ))
}

// svg data bar
fn data_bar(down_per: u32, width: u32, height: u32, is_big_bar: bool) -> String {
    format!(
        "<div width=\"{width}\" height=\"{height}\" style=\"overflow: hidden;\">\n\
            <svg{} width=\"{width}\" height=\"{height}\">\n\
                <rect x=\"0\" y=\"0\" width=\"{width}\" height=\"{height}\" style=\"fill:cyan\" />\n\
                <rect x=\"0\" y=\"0\" width=\"{}\" height=\"{height}\" style=\"fill:magenta\" />\n\
            </svg>\n\
        </div>",
        if is_big_bar { " id=\"du_bar\"" } else { "" },
        if down_per == 0 { width } else { down_per * (width / 100) }
    )
}

// pre-formatted HTML response
fn html_response<T: Display>(body: T, iframe: bool, clearfix: bool) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(
            if iframe {
                format!(
                    "<!DOCTYPE html>\n\
                    <html>\n\
                        <meta charset=\"utf-8\">\n\
                        <head>\n\
                            <link rel=\"stylesheet\" href=\"/iframe.css\">\n\
                        </head>\n\
                        {body}\n\
                    </html>"
                )
            }
            else {
                format!(
                    "<!DOCTYPE html>\n\
                    <html>\n\
                        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\
                        <meta charset=\"utf-8\">\n\
                        <link rel=\"icon\" type=\"image/png\" href=\"favicon.png\"/>\n\
                        <link rel=\"stylesheet\" href=\"/styles.css\">\n\
                        <title>T-Mobile Gateway Monitor</title>\n\
                        <div id=\"navbar\" style=\"text-align:center;\">\n\
                            <a href=\"/\">Home</a>\n\
                            <a href=\"/adv_stats\">Advanced Stats</a>\n\
                            <a href=\"/diag\">Diagnostics</a>\n\
                            <a href=\"/config\">Settings</a>\n\
                            <a href=\"/help\">Help</a>\n\
                        </div>\n\
                        <div id=\"body\"{}>\n\
                            {body}\n\
                        </div>\n\
                        <div id=\"foot\">\n\
                            <p>Copyright 2022 Aercloud Systems<br>Some Rights Reserved</p>\n\
                        </div>\n\
                        <script>\
                            var hide=document.getElementsByClassName(\"hide_button\");\
                            for(var i=0;i<hide.length;i++){{\
                                hide[i].addEventListener(\"click\",function(){{\
                                    var content=this.nextElementSibling;\
                                    if(content.style.display==\"block\"||!content.style.display){{\
                                        content.style.display=\"none\";\
                                        this.style.backgroundColor=\"rgba(61,65,83,0.25)\";\
                                    }}\
                                    else{{\
                                        content.style.display=\"block\";\
                                        this.style.backgroundColor=\"rgba(0,0,0,0)\";\
                                    }}\
                                }});\
                            }}\
                        </script>\n\
                    </html>",
                    if clearfix { " class=\"clearfix\"" } else { "" }
                )
            })
}